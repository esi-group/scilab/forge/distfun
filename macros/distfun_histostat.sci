// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [M,V]=distfun_histostat(h)
    // Histogram mean and variance
    //  
    // Calling Sequence
    //   M=distfun_histostat(h)
    //   [M,V]=distfun_histostat(h)
    //
    // Parameters
    // h : an histogram object
    //   M : a matrix of doubles, the mean
    //   V : a matrix of doubles, the variance
    //
    // Description
    // Computes statistics from the histogram.
    //
    // The h object must be created with distfun_histocreate.
    //
    //    The Mean and Variance of the Histogram Distribution are:
    //
    //<latex>
    //\begin{eqnarray}
    // M &=& \frac{1}{2S} \sum_{i=1}^{nbins} f_i l_i (e_i+e_{i+1}) \\
    // V &=& \frac{1}{3S} \sum_{i=1}^{nbins} f_i l_i (e_i^2+e_i e_{i+1}+e_{i+1}^2),
    //\end{eqnarray}
    //</latex>
    //
    // where 
    //
    //<latex>
    //S= \sum_{i=1}^{nbins} f_i l_i,
    //</latex>    
    //
    // <latex>e_i</latex> are the edges of the bins, 
    // <latex>f_i</latex> are the heights of the histogram and 
    // <latex>l_i=e_{i+1}-e_i</latex> are the lengths of the bins.
    //
    // Examples
    // m=1000;
    // data=distfun_normrnd(0,1,m,1);
    // h=distfun_histocreate("data",data);
    // [M,V]=distfun_histostat(h)
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_histostat" , rhs , 1:1 )
    apifun_checklhs ( "distfun_histostat" , lhs , 0:2 )
    //
    // Check Type
    apifun_checktype ( "distfun_histostat" , h , "h" , 1 , "st" )
    //
    // Check Size
    // Nothing to do
    //
    // Check content
    // Nothing to do

    // Proceed...
    e=h.edges
    p=h.pdf
    w=e(2:$)-e(1:$-1) // The lengths of the bins
    c=0.5*(e(2:$)+e(1:$-1)) // The centers of the bins
    S=sum(p.*w)
    M=sum(p.*w.*c)/S
    s=(1/3.)*(e(1:$-1).^2+e(1:$-1).*e(2:$)+e(2:$).^2)+M*(M-2*c)
    V=sum(p.*w.*s)/S
endfunction
