// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function y=distfun_histopdf(x,h)
    // Histogram PDF
    //  
    // Calling Sequence
    //   y=distfun_histopdf(x,h)
    //
    // Parameters
    // x : a matrix of doubles, the outcome
    // h : an histogram object
    // y : a matrix of doubles, the density
    //
    // Description
    // Computes the probability distribution function of the 
    // Histogram.
    //
    // The h object must be created with distfun_histocreate.
    //
    // Examples
    // m=1000;
    // data=distfun_normrnd(0,1,m,1);
    // h=distfun_histocreate("data",data);
    // x=linspace(-4,4);
    // y=distfun_histopdf(x,h);
    // scf();
    // plot(x,y)
    // ynorm=distfun_normpdf(x,0,1);
    // plot(x,ynorm)
    // legend(["Histogram","Exact"]);
    // xlabel("x")
    // ylabel("Density")
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_histopdf" , rhs , 2:2 )
    apifun_checklhs ( "distfun_histopdf" , lhs , 0:1 )
    //
    // Check Type
    apifun_checktype ( "distfun_histopdf" , x , "x" , 1 , "constant" )
    apifun_checktype ( "distfun_histopdf" , h , "h" , 2 , "st" )
    //
    // Check Size
    // Nothing to do
    //
    // Check content
    // Nothing to do

    // Proceed...
    y=zeros(x)
    ibins=dsearch(x,h.edges)
    // j=ibins(i) is so that edges(j)<x(i) & x(i)<=edges(j+1)
    jindices=find(ibins>0)
    pdf=h.pdf(ibins(jindices))
    // Reformat the matrix shape
    y(jindices)=matrix(pdf,size(x(jindices)))
endfunction
