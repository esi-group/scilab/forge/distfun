// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function x=distfun_histoinv(varargin)
    // Histogram inverse CDF
    //  
    // Calling Sequence
    //   x=distfun_histoinv(p,h)
    //   x=distfun_histoinv(p,h,lowertail)
    //
    // Parameters
    // p : a matrix of doubles, the probability
    // h : an histogram object
    // lowertail : a 1-by-1 matrix of booleans, the tail (default lowertail=%t). If lowertail is true (the default), then considers P(X<=x) otherwise P(X>x).
    // x : a matrix of doubles, the outcome
    //
    // Description
    // Computes the inverse cumulated probability distribution function of the 
    // Histogram.
    //
    // The h object must be created with distfun_histocreate.
    //
    // Examples
    // m=1000;
    // data=distfun_normrnd(0,1,m,1);
    // h=distfun_histocreate("data",data);
    // p=linspace(0.001,0.999);
    // x=distfun_histoinv(p,h);
    // scf();
    // plot(p,x,"r-")
    // xnorm=distfun_norminv(p,0,1);
    // plot(p,xnorm,"b-")
    // legend(["Histogram","Exact"],"in_upper_left");
    // xtitle("Histogram")
    // xlabel("p")
    // ylabel("x|P(X<x)=p")
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_histoinv" , rhs , 2:3 )
    apifun_checklhs ( "distfun_histoinv" , lhs , 0:1 )
    //
    p=varargin(1)
    h=varargin(2)
    lowertail = apifun_argindefault(varargin,3,%t)
    //
    // Check Type
    apifun_checktype ( "distfun_histoinv" , p , "p" , 1 , "constant" )
    apifun_checktype ( "distfun_histoinv" , h , "h" , 2 , "st" )
    apifun_checktype("distfun_histoinv",lowertail,"lowertail",3,"boolean")
    //
    // Check Size
    apifun_checkscalar("distfun_histoinv",lowertail,"lowertail",3)
    //
    // Check content
    apifun_checkrange( "distfun_histoinv" , p , "p" , 1 , 0, 1 )

    // Proceed...
    if (~lowertail) then
        p=1-p
    end
    x=zeros(p)
    ibins=dsearch(p,h.cdf)
    //
    jindices=find(ibins==0)
    x(jindices)=h.edges(1)
    //
    jindices=find(ibins>0)
    q=h.edges(ibins(jindices)+1)
    // Reformat the matrix shape
    x(jindices)=matrix(q,size(p(jindices)))
endfunction
