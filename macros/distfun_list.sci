function s=distfun_list(varargin)
    // Returns the list of available distributions
    //
    // Calling Sequence
    //   s=distfun_list("longnames")
    //   s=distfun_list("shortnames")
    //
    // Parameters
    //   s : a matrix of strings, available distributions in long or short format
    //
    // Description
    // Inquires about the available distributions.
    //
    // distfun_list("longnames") returns a matrix of strings containing 
    // all the available distributions, in long format, human readable.
    // e.g. : "Uniform", "Normal", etc...
    //
    // distfun_list("shortnames") returns a matrix of strings containing 
    // all the available distributions, in short format, 
    // e.g. : "unif", "norm", etc...
    //
    // Examples
    // s=distfun_list("longnames")
    // s=distfun_list("shortnames")
    //
    // // Print the map
    // sn=distfun_list("longnames");
    // ln=distfun_list("shortnames");
    // mprintf("%-25s : ""%s""\n",[sn,ln]);
    //
    // Authors
    //   Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_list" , rhs , 1 )
    apifun_checklhs ( "distfun_list" , lhs , 0:1 )
    //
    purpose = varargin(1)
    //
    // Check type
    apifun_checktype ( "distfun_list" , purpose , "purpose" , 1 , "string" )
    //
    // Check size
    apifun_checkscalar ( "distfun_list" , purpose , "purpose" , 1 )
    //
    // Check content
    alloptions=[
    "longnames"
    "shortnames"
    ]
    apifun_checkoption( "distfun_list" , purpose , "purpose" , 1 , alloptions)
    //
    // Proceed...
    map=[
    "Beta" "beta"
    "Binomial" "bino"
    "Chi-Squared" "chi2"
    "Extreme Value" "ev"
    "Exponential" "exp"
    "F" "f"
    "Gamma" "gam"
    "Geometric" "geo"
    "Hypergeometric" "hyge"
    "Kolmogorov-Smirnov" "ks"
    "LogNormal" "logn"
    "LogUniform" "logu"
    "Multinomial" "mn"
    "Multivariate Normal" "mvn"
    "Negative Binomial" "nbin"
    "Noncentral F" "ncf"
    "Noncentral T" "nct"
    "Noncentral Chi-Squared" "ncx2"
    "Normal" "norm"
    "Poisson" "poi"
    "T" "t"
    "Truncated Normal" "tnorm"
    "Uniform Discrete" "unid"
    "Uniform" "unif"
    "Weibull" "wbl"
    ]
    select purpose
    case "longnames" then
        s=map(:,1)
    case "shortnames" then
        s=map(:,2)
    end
endfunction
