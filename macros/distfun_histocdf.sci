// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function p=distfun_histocdf(varargin)
    // Histogram CDF
    //  
    // Calling Sequence
    //   p=distfun_histocdf(x,h)
    //   p=distfun_histocdf(x,h,lowertail)
    //
    // Parameters
    // x : a matrix of doubles, the outcome
    // h : an histogram object
    // lowertail : a 1-by-1 matrix of booleans, the tail (default lowertail=%t). If lowertail is true (the default), then considers P(X<=x) otherwise P(X>x).
    // p : a matrix of doubles, the probability
    //
    // Description
    // Computes the cumulated probability distribution function of the 
    // Histogram.
    //
    // The h object must be created with distfun_histocreate.
    //
    // Examples
    // m=1000;
    // data=distfun_normrnd(0,1,m,1);
    // h=distfun_histocreate("data",data);
    // x=linspace(-4,4);
    // y=distfun_histocdf(x,h);
    // scf();
    // plot(x,y,"r-")
    // ynorm=distfun_normcdf(x,0,1);
    // plot(x,ynorm,"b-")
    // legend(["Histogram","Exact"]);
    // xlabel("x")
    // ylabel("Probability")
    //
    // Authors
    // Copyright (C) 2014 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "distfun_histocdf" , rhs , 2:3 )
    apifun_checklhs ( "distfun_histocdf" , lhs , 0:1 )
    //
    x=varargin(1)
    h=varargin(2)
    lowertail = apifun_argindefault(varargin,3,%t)
    //
    // Check Type
    apifun_checktype ( "distfun_histocdf" , x , "x" , 1 , "constant" )
    apifun_checktype ( "distfun_histocdf" , h , "h" , 2 , "st" )
    apifun_checktype("distfun_histocdf",lowertail,"lowertail",3,"boolean")
    //
    // Check Size
    apifun_checkscalar("distfun_histocdf",lowertail,"lowertail",3)
    //
    // Check content
    // Nothing to do

    // Proceed...
    p=zeros(x)
    ibins=dsearch(x,h.edges)
    // j=ibins(i) est tel que edges(j)<x(i) & x(i)<=edges(j+1)
    jindices=find(ibins>0)
    // Compute CDF
    cdf=h.cdf(ibins(jindices))
    // Reformat the matrix shape
    p(jindices)=matrix(cdf,size(x(jindices)))
    // Manage too large x
    p(x>h.edges($))=1.
    // 
    if (~lowertail) then
        p=1-p
    end
endfunction
