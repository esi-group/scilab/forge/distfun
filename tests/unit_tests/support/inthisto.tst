// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- NO CHECK ERROR OUTPUT -->

pr=0.7;
N=10000;
R=distfun_geornd(pr,1,N);
f = scf();
H = distfun_inthisto(R);
assert_checkequal(size(H,"c"),2);
assert_checktrue(H(:,1)>=0.);
assert_checktrue(H(:,2)>=0.);
assert_checktrue(H(:,2)<=1.);
assert_checkalmostequal(floor(H(:,1)), H(:,1), %eps);
assert_checkalmostequal(sum(H(:,2)), 1., %eps);
close(f);
//
f = scf();
H = distfun_inthisto(R,%f);
assert_checkequal(size(H,"c"),2);
assert_checktrue(H(:,1)>=0.);
assert_checktrue(H(:,2)>=0.);
assert_checktrue(H(:,2)<=N);
assert_checkalmostequal(floor(H),H, %eps);
assert_checkalmostequal(sum(H(:,2)), N, %eps);
close(f);

