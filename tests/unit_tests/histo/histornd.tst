// Copyright (C) 2014 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

m=1000;
data=distfun_normrnd(0,1,m,1);
h=distfun_histocreate("data",data);
//
x=distfun_histornd(h,1000,1);
assert_checkalmostequal(mean(x),0,[],0.5);
assert_checkalmostequal(stdev(x),1,[],0.5);
