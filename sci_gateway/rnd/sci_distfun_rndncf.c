/*
* Copyright (C)  2012 - 2014 - Michael Baudin
* Copyright (C)  2001 - Bruno Pincon
* Copyright (C) ENPC - jpc@cermics.enpc.fr
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/

#include <string.h>
#include <math.h>

// From Distfun:
#include "gwsupport.h"
#include "cdflib.h"
#include "gw_distfunrnd.h"
#include "gw_distfunrnd_support.h" 

// From Scilab:
#include "api_scilab.h"



int sci_distfun_rndncf(GW_PARAMETERS)
{
	int rRows,rCols;
	int ma, na;
	int mb, nb;
	int mc, nc;
	int readFlag;
	double * pa = NULL;
	double * pb = NULL;
	double * pc = NULL;

	CheckInputArgument(pvApiCtx,3,5);
	CheckOutputArgument(pvApiCtx,1,1);

    // Arg #1 : a
	readFlag = gwsupport_GetRealMatrixOfDoubles( fname, 1, &pa, &ma, &na,pvApiCtx);
	if ( readFlag == GWSUPPORT_ERROR)
	{ 
		return 0;
	}
	// Check that a >= 1.
	readFlag = gwsupport_CheckDoubleMatrixGreaterOrEqual (fname, 1, pa, ma, na, 1.0);
	if ( readFlag == GWSUPPORT_ERROR)
	{ 
		return 0;
	}
    // Arg #2 : b>0
	readFlag = gwsupport_GetMatrixOfDoublesGreaterThanZero( fname, 2, &pb, &mb, &nb,pvApiCtx);
	if ( readFlag == GWSUPPORT_ERROR)
	{ 
		return 0;
	}
    // Arg #3 : c>=0
	readFlag = gwsupport_GetMatrixOfDoublesGreaterOrEqualThanZero( fname, 3, &pc, &mc, &nc,pvApiCtx);
	if ( readFlag == GWSUPPORT_ERROR)
	{ 
		return 0;
	}
	// Get [m,n], if any.
	readFlag = distfun_GetMNV_ABC ( fname, *getNbInputArgument(pvApiCtx), &rRows, &rCols,pvApiCtx);
	if ( readFlag == GWSUPPORT_ERROR)
	{ 
		return 0;
	}
    // Compute the result (if necessary, expand the arguments a and b and C)
	readFlag = distfun_computeRandgenABC (fname, ma, na, pa, mb, nb, pb, mc, nc, pc, rRows, rCols, cdflib_ncfrnd,pvApiCtx);
	if ( readFlag == GWSUPPORT_ERROR)
	{ 
		return 0;
	}
	LhsVar(1) = Rhs+1;

	return 0;
}
