// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// This file is released into the public domain
// ====================================================================

function distfun_builderGatewayC()
    gateway_path = get_absolute_file_path("builder_gateway_driver.sce");

    libname = "distfundrivergateway";
    namelist = [
    "distfun_startup"   "sci_distfun_startup"
    "distfun_verboseset"   "sci_distfun_verboseset"
    ];
    files = [
    "sci_distfun_startup.c"
    "sci_distfun_verboseset.c"
    ];

    ldflags = ""

    if (getos() == "Windows") then      
        cflags = "-DWIN32";
    else
        cflags = "-O0 -g";
    end 
    cflags = cflags + " " + ..
        ilib_include_flag(gateway_path) + ..    
        ilib_include_flag(fullpath(gateway_path + "../../src/cdflib")) + .. 
        ilib_include_flag(fullpath(gateway_path + "../../src/unifrng")) + ..                
        ilib_include_flag(fullpath(gateway_path + "../../src/gwsupport"));          
        
    // Caution : the order matters !
    libs = [
    "../../src/gwsupport/libgwsupport"
    "../../src/unifrng/libunifrng"
    "../../src/cdflib/libcdflib"
    ];

    tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags, cflags);

endfunction
distfun_builderGatewayC();
clear distfun_builderGatewayC;

