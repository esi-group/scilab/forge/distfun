
// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

//
// gw_distfunurng_support.h
//   Header for the C gateway support functions for DISTFUN/URNG
//
#ifndef __SCI_DISTFUN_URNG_GWSUPPORT_H__
#define __SCI_DISTFUN_URNG_GWSUPPORT_H__


/* ==================================================================== */

#endif /* __SCI_DISTFUN_URNG_GWSUPPORT_H__ */
